namespace VoteApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Contestants", "Group");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contestants", "Group", c => c.Int(nullable: false));
        }
    }
}
