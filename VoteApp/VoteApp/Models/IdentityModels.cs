﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VoteApp.Models
{
    public class Department
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Contestant> Contestants { get; set; }
    }

    public class Contest
    {
        [Key]
        public string Id { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime EndDate { get; set; }
    }

    public class Img
    {
        [Key]
        public string Id { get; set; }
        public string FileName { get; set; }

        public virtual Thumbnail cThumbnail { get; set; }

        public virtual Contestant Contestant { get; set; }
    }

    public class Thumbnail
    {
        [Key]
        public string Id { get; set; }
        public string FileName { get; set; }

        public virtual Img cImg { get; set; }
    }


    public class Vote
    {
        public string Id { get; set; }
        public int VotedCount { get; set; }
        public DateTime Time { get; set; }

        public virtual Contestant VotedContestant { get; set; }
        public virtual AppUser VotedUser { get; set; }
    }

    public class Contestant
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string MS { get; set; }

        [Required]
        public string Name { get; set; }

        [Range(3, 99)]
        public int Age { get; set; }

        public bool Sex { get; set; }
        public bool Couple { get; set; }

        public int VotedCount { get; set; }
        public int ViewedCount { get; set; }

        public int Group { get; set; }

        public virtual Department cDepartment { get; set; }
        public virtual ICollection<Img> Imgs { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }

    public class AppUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public virtual ICollection<Vote> Votes { get; set; }
    }

    public class AppDb : IdentityDbContext<AppUser>
    {
        public AppDb()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static AppDb Create()
        {
            return new AppDb();
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Contestant>().HasMany(s => s.Votes).WithRequired(s => s.VotedContestant).WillCascadeOnDelete(true);
            modelBuilder.Entity<Img>().HasOptional(s => s.cThumbnail).WithRequired(s => s.cImg).WillCascadeOnDelete(true);
        }

        public DbSet<Contest> Contests { get; set; }
        public DbSet<Contestant> Contestants { get; set; }
        public DbSet<Img> Imgs { get; set; }
        public DbSet<Thumbnail> Thumbnails { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Vote> Votes { get; set; }
    }
}