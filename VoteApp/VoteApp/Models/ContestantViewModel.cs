﻿using System.ComponentModel.DataAnnotations;

namespace VoteApp.Models
{
    public class ContestantViewModel
    {
        [Required]
        public string MS { get; set; }

        [Required]
        public string Name { get; set; }

        [Range(3, 99)]
        public int Age { get; set; }

        public bool Sex { get; set; }
        public bool Couple { get; set; }

        public string DepartmentId { get; set; }
    }
}