﻿using System.Collections.Generic;

namespace VoteApp.Models
{
    public class RankViewModal
    {
        public Dictionary<int, List<Contestant>> Contestants { get; set; }
        public Dictionary<int, Vote> Voter { get; set; }
    }
}