﻿using System;
using System.Collections.Generic;

namespace VoteApp.Models
{
    public class VoteViewModel
    {
        public string Id { get; set; }

        public string MS { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public bool Sex { get; set; }
        public bool Couple { get; set; }

        public int VotedCount { get; set; }
        public int ViewedCount { get; set; }
    }
    public class VoteDetailViewModel
    {
        public Contestant Contestant { get; set; }
        public Dictionary<int, string> LikedGroups { get; set; }
        public Dictionary<string, int> LikePerDay { get; set; }
    }
}