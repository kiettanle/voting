﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteApp.Models;

namespace VoteApp.Controllers
{
    public class RankController : Controller
    {
        AppDb db = AppDb.Create();

        [Route("kết-quả")]
        public ActionResult Index()
        {
            RankViewModal model = new RankViewModal();
            model.Contestants = new Dictionary<int, List<Contestant>>();
            model.Voter = new Dictionary<int, Vote>();

            var groups = db.Contestants.GroupBy(s => s.Group);
            List<Contestant> hightestVote = new List<Contestant>();
            foreach (var group in groups)
            {
                List<Contestant> contestants = group.OrderByDescending(s => s.VotedCount).ToList();
                model.Contestants.Add(group.Key, contestants);
                hightestVote.Add(contestants[0]);
            }
            foreach (var c in hightestVote)
            {
                if (c.VotedCount > 0)
                {
                    int min = c.Votes.Min(s => Math.Abs(s.VotedCount - c.VotedCount));
                    var user = c.Votes.Where(s => Math.Abs(s.VotedCount - c.VotedCount) == min).OrderBy(s => s.Time).ElementAt(0);
                    model.Voter.Add(c.Group, user);
                }
            }
            return View(model);
        }
    }
}