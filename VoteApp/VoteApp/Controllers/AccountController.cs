﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using VoteApp.Models;

namespace VoteApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private AppDb Db = AppDb.Create();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        //[Route("đăng-nhập")] trinh duyet ko cho phep unicode tren url. no se ma hoa thanh cai khac
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        //[Route("đăng-nhập")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (model.ResetPassword)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError("", "Emai không tồn tại");
                    return View(model);
                }
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "[Vote App] Reset Password (noreply)", "B&#7841;n vui l&#242;ng click v&#224;o <a href=\"" + callbackUrl + "\">link n&#224;y</a> &#273;&#7875; reset m&#7853;t kh&#7849;u.");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            if (!ModelState.IsValid)
                return View(model);

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    if (string.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Vote");
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Đăng nhập thất bại");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await UserManager.FindByEmailAsync(model.Email) != null)
                {
                    ModelState.AddModelError("", $"Email {model.Email} đã đăng ký. Nếu không phải do bạn đăng ký. Hãy liên hệ với admin để khôi phục tài khoản.");
                    return View(model);
                }

                var user = new AppUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id,
                      "[Vote App] Xác thực email (noreply)", $"Ch&#224;o b&#7841;n,<br/> Vui l&#242;ng Click v&#224;o &#273;&#432;&#7901;ng link  <a href=\"{callbackUrl}\">n&#224;y</a> &#273;&#7875; x&#225;c th&#7921;c t&#224;i kho&#7843;n.<br/>Xin c&#7843;m &#417;n,");
                    return View("Info");
                }
                AddErrors(result);
            }
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return View("ForgotPasswordConfirmation");
                }
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "B&#7841;n vui l&#242;ng click v&#224;o <a href=\"" + callbackUrl + "\">link n&#224;y</a> &#273;&#7875; reset m&#7853;t kh&#7849;u.");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            ViewBag.Code = code;
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                var user2 = Db.Users.Single(s => s.Id == user.Id);
                user2.EmailConfirmed = true;
                await Db.SaveChangesAsync();
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }


        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }
            if (loginInfo.Email == null)
            {

                loginInfo.Email = loginInfo.DefaultUserName +"@"+loginInfo.Login.LoginProvider+".com";
                loginInfo.DefaultUserName = loginInfo.DefaultUserName + "@" + loginInfo.Login.LoginProvider + ".com";

            }
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("Index", "Vote");
                case SignInStatus.Failure:
                default:
                    AppUser user = await UserManager.FindByEmailAsync(loginInfo.Email);
                    if (user == null)
                    {
                        user = new AppUser { UserName = loginInfo.DefaultUserName, Email = loginInfo.Email };
                        var createResult = await UserManager.CreateAsync(user);
                        if (!createResult.Succeeded)
                            return View("ExternalLoginFailure");
                    }
                    if (user.Logins != null)
                        user.Logins.Clear();
                    var loginResult = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                    if (loginResult.Succeeded)
                    {
                        await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
                        return RedirectToAction("Index", "Vote");
                    }
                    return View("ExternalLoginFailure");
            }
        }


        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult XinLamAdmin()
        {
            using (AppDb db = AppDb.Create())
            {
                IdentityRole adminRole = db.Roles.SingleOrDefault(s => s.Name == "Admin");
                if (adminRole == null)
                {
                    adminRole = new IdentityRole("Admin");
                    db.Roles.Add(adminRole);
                    db.SaveChanges();
                }
                if (adminRole.Users.Count == 0)
                {
                    UserManager.AddToRole(User.Identity.GetUserId(), "Admin");
                }
                if (User.IsInRole("Admin"))
                    return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Management()
        {
            var model = Db.Users.Where(s => s.EmailConfirmed == false && !string.IsNullOrEmpty(s.PasswordHash)).ToList();
            return View(model);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Active(string username)
        {
            var user = Db.Users.SingleOrDefault(s => s.UserName == username);
            if (user != null)
            {
                user.EmailConfirmed = true;
                Db.SaveChanges();
            }
            return RedirectToAction("Management");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}