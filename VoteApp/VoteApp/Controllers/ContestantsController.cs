﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using VoteApp.Models;

namespace VoteApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContestantsController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Contestants
        public async Task<ActionResult> Index()
        {
            return View(await db.Contestants.ToListAsync());
        }

        // GET: Contestants/Create
        public ActionResult Create()
        {
            ViewBag.Departments = db.Departments.ToList();
            return View();
        }

        // POST: Contestants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ContestantViewModel model)
        {
            ViewBag.Departments = db.Departments.ToList();
            if (ModelState.IsValid)
            {
                Contestant contestant = new Contestant()
                {
                    MS = model.MS,
                    Age = model.Age,
                    Couple = model.Couple,
                    cDepartment = db.Departments.Single(s => s.Id == model.DepartmentId),
                    Name = model.Name,
                    Sex = model.Sex
                };
                if (contestant.Couple)
                    contestant.Group = 2;
                else if (contestant.Sex)
                    contestant.Group = (contestant.Age > 40 ? 1 : 0);
                else
                    contestant.Group = (contestant.Age > 30 ? 1 : 0);

                contestant.Id = Guid.NewGuid().ToString();
                contestant.ViewedCount = 0;
                contestant.VotedCount = 0;
                contestant.Imgs = new List<Img>();
                if (Request.Files.Count < 6)
                {
                    ModelState.AddModelError("", "Không thể không có hình ảnh");
                    return View(model);
                }

                for (int i = 0; i < 6; i++)
                    if (Request.Files[i].ContentLength == 0)
                    {
                        ModelState.AddModelError("", "Không thể không có hình ảnh");
                        return View(model);
                    }
                try
                {
                    for (int i = 0; i < 3; i++)
                    {
                        string ext = Request.Files[i].FileName.Substring(Request.Files[i].FileName.Length - 4, 4);
                        string fileName = Guid.NewGuid().ToString();
                        Request.Files[i].SaveAs(Server.MapPath(@"~/Content/Uploads/" + fileName + ext));
                        contestant.Imgs.Add(new Img()
                        {
                            Id = fileName,
                            FileName = (@"/Content/Uploads/" + fileName + ext)
                        });
                    }
                    for (int i = 3; i < 6; i++)
                    {
                        string ext = Request.Files[i].FileName.Substring(Request.Files[i].FileName.Length - 4, 4);
                        string fileName = Guid.NewGuid().ToString();
                        Request.Files[i].SaveAs(Server.MapPath(@"~/Content/Thumbnails/" + fileName + ext));
                        contestant.Imgs.ElementAt(i-3).cThumbnail = new Thumbnail()
                        {
                            Id = fileName,
                            FileName = (@"/Content/Thumbnails/" + fileName + ext)
                        };
                    }
                    db.Contestants.Add(contestant);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ModelState.AddModelError("", "Upload thất bại");
                    foreach (var file in contestant.Imgs)
                        if (System.IO.File.Exists(Server.MapPath(file.FileName)))
                            System.IO.File.Delete(Server.MapPath(file.FileName));
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: Contestants/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contestant contestant = await db.Contestants.FindAsync(id);
            if (contestant == null)
            {
                return HttpNotFound();
            }
            return View(contestant);
        }

        // POST: Contestants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,MS,Name,Description,VotedCount,ViewdCount")] Contestant contestant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contestant).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(contestant);
        }

        // GET: Contestants/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contestant contestant = await db.Contestants.FindAsync(id);
            if (contestant == null)
            {
                return HttpNotFound();
            }
            return View(contestant);
        }

        // POST: Contestants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Contestant contestant = await db.Contestants.FindAsync(id);
            foreach (var img in contestant.Imgs)
                if (System.IO.File.Exists(Server.MapPath(img.FileName)))
                    System.IO.File.Delete(Server.MapPath(img.FileName));
            db.Contestants.Remove(contestant);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
