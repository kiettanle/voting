﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteApp.Models;

namespace VoteApp.Controllers
{
    public class ContestController : Controller
    {
        AppDb db = AppDb.Create();

        // GET: Contest
        public ActionResult Index()
        {
            var contest = db.Contests.FirstOrDefault();
            if (contest == null)
            {
                contest = new Contest()
                {
                    Id = Guid.NewGuid().ToString(),
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now + TimeSpan.FromDays(30)
                };
                db.Contests.Add(contest);
                db.SaveChanges();
            }
            return View(contest);
        }

        [HttpPost]
        public ActionResult Index(Contest newContest)
        {
            var contest = db.Contests.SingleOrDefault(s => s.Id == newContest.Id);
            if (contest != null)
            {
                contest.Id = newContest.Id;
                contest.StartDate = newContest.StartDate;
                contest.EndDate = newContest.EndDate;
                db.Entry(contest).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                newContest.Id = Guid.NewGuid().ToString();
                db.Contests.Add(newContest);
            }
            db.SaveChanges();
            return View(contest);
        }
    }
}