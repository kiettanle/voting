﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList;
using VoteApp.Models;

namespace VoteApp.Controllers
{
    public class VoteController : Controller
    {
        AppDb db = new AppDb();

        [Route("thí-sinh/{page?}")]
        public ActionResult Index(int page = 1)
        {
            Dictionary<int, string> likegroup = new Dictionary<int, string>();

            if (Request.IsAuthenticated)
            {
                var user = db.Users.Single(s => s.UserName == User.Identity.Name);
                foreach (var vote in user.Votes)
                    likegroup.Add(vote.VotedContestant.Group, vote.VotedContestant.Id);
            }
            ViewBag.LikedGroups = likegroup;
            int minute = DateTime.Now.Second; //  1-> 606 0/ mot phut thay doi  
            float percent = 1 / 10.0f * minute; //
            if (percent >= 1)
                percent -= 1;
            var data = db.Contestants.ToList();
            int offset = (int)(data.Count * percent); //tat insert giup e
            var newdata = data.Skip(offset).ToList();
            newdata.AddRange(data.Take(offset)); //cho e 5p
            PagedList<Contestant> model = new PagedList<Contestant>(newdata, page, 10);
            return View(model);
        }

     
        [Route("thí-sinh/chi-tiết/{id?}")]
        public ActionResult Detail(string id)
        {
            VoteDetailViewModel model = new VoteDetailViewModel();

            model.Contestant = db.Contestants.SingleOrDefault(s => s.Id == id || s.MS.ToLower() == id.ToLower());
            if (model.Contestant == null)
                return RedirectToAction("Index");

            var days = model.Contestant.Votes.GroupBy(s => s.Time.ToShortDateString()).ToList();
            model.LikePerDay = new Dictionary<string, int>();
            foreach (var day in days)
                model.LikePerDay.Add(day.Key, day.Count());
            model.LikedGroups = new Dictionary<int, string>();
            if (Request.IsAuthenticated)
            {
                var votes = db.Users.Single(s => s.UserName == User.Identity.Name).Votes;
                foreach (var vote in votes)
                    model.LikedGroups.Add(vote.VotedContestant.Group, vote.VotedContestant.Id);
            }

            return View(model);
        }

        [Authorize]
        [Route("bình-chọn/{id}/{count?}")]
        public async Task<JsonResult> Submit(string id, int count = 0)
        {
            Contest contest = db.Contests.FirstOrDefault();
            if (contest == null || DateTime.Now < contest.StartDate || DateTime.Now > contest.EndDate)
                return Json(new { error = "Không trong thời gian bình chọn" }, JsonRequestBehavior.AllowGet);

            Contestant contestant = db.Contestants.SingleOrDefault(s => s.Id == id);
            if (contestant == null)
                return Json(new { error = "Không tìm thấy ảnh này" }, JsonRequestBehavior.AllowGet);
            var user = db.Users.Single(s => s.UserName == User.Identity.Name);
            if (user.PasswordHash != null && user.EmailConfirmed == false)
                return Json(new { error = "Tài khoản email chưa xác thực. Bạn phải vào tài khoản email đã đăng ký để xác thực." }, JsonRequestBehavior.AllowGet);
            bool like = false;

            var vote = contestant.Votes.SingleOrDefault(s => s.VotedUser.UserName == user.UserName);
            if (vote != null)
            {
                db.Votes.Remove(vote);
                contestant.VotedCount--;
            }
            else
            {
                if (user.Votes.Any(s => s.VotedContestant.Group == contestant.Group))
                    return Json(new { error = "Bạn đã bình chọn cho một thí sinh khác cùng bản này" }, JsonRequestBehavior.AllowGet);

                contestant.Votes.Add(new Vote()
                {
                    Id = Guid.NewGuid().ToString(),
                    Time = DateTime.Now,
                    VotedCount = count,
                    VotedUser = user
                });
                like = true;
                contestant.VotedCount++;
            }

            db.Entry(contestant).State = System.Data.Entity.EntityState.Modified;
            await db.SaveChangesAsync();
            return Json(new { l = like, c = contestant.VotedCount }, JsonRequestBehavior.AllowGet);
        }

        [Route("xem/{id?}")]
        public async Task<JsonResult> ViewImg(string id)
        {
            Img c = await db.Imgs.FindAsync(id);
            if (c == null)
                return Json(new { error = "Không tìm thấy ảnh này" }, JsonRequestBehavior.AllowGet);
            DateTime? last = Session[id] as DateTime?;
            if (last == null || DateTime.Now - last > TimeSpan.FromHours(8))
            {
                Session.Add(id, DateTime.Now);
                c.Contestant.ViewedCount++;
                db.Entry(c.Contestant).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}