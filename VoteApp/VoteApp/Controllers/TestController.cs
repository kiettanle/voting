﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VoteApp.Controllers
{
    public class TestController : Controller
    {
        private Dictionary<string, int> data = new Dictionary<string, int>()
        {
            {"A",15 },
            {"B",16 },
            {"C",17 },
            {"D",18 },
        };

        // GET: Test
        public ActionResult Index(int fillter = 0)
        {
            ViewBag.fillter = fillter;
            return View();
        }

        public ActionResult GetData(int fillter = 0)
        {
            List<string> models = new List<string>();
            ViewBag.fillter = fillter;

            switch (fillter)
            {
                case 0:
                    foreach (var item in data)
                        models.Add(item.Key);
                    break;
                case 1:
                    foreach (var item in data)
                        if (item.Value % 2 == 0)
                            models.Add(item.Key);
                    break;
                default:
                    foreach (var item in data)
                        if (item.Value % 2 != 0)
                            models.Add(item.Key);
                    break;
            }
            return PartialView("_Data", models);
        }
    }
}